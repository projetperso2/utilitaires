
# Petit projet regroupant des outils classiques

MergePDF, PDF to PNG etc... Pleins d'outils pratiques que nous utilisons tous les jours.
Mais pour des documents un peu sensibles ça devient vite compliqué.
Ces sites sont complétement opaques et on ne sait absolument pas si nos documents sont transmis aux sites.
J'ai donc recréé ces outils sur ce repo.

# Requirements

- Python 3.7 +
- Pip

```shell
python -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt
```
