from PyPDF2 import PdfMerger
from datetime import date, datetime

from os import listdir
from os.path import isfile, join
import sys

SOURCE = "./pdf_to_merge/"
TARGET = "./result/"

list_files = [f for f in listdir(SOURCE) if isfile(join(SOURCE, f))]
final_files = [join(SOURCE, f) for f in list_files]

merger = PdfMerger()

for pdf in final_files:
    merger.append(pdf)

final_file_name = datetime.now().strftime("%d-%m-%Y_%H-%M-%S")+".pdf"
merger.write(join(TARGET, final_file_name))
merger.close()